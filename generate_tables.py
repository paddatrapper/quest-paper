###########################################################################
# GenerateTables is Copyright (C) 2019 Kyle Robbertze <RBBKYL001@myuct.ac.za>
#
# GenerateTables is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# GenerateTables is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GenerateTables. If not, see <http://www.gnu.org/licenses/>.
###########################################################################
import string

def parse_csv(filename):
    raw_results = []
    with open(filename) as f:
        for line in f:
            if line.startswith('#'):
                continue
            values = line.split(',')
            page = int(values[0])
            total_relevant = int(values[1])
            true_positive = int(values[2])
            false_positive = int(values[3])
            precision = true_positive / (true_positive + false_positive) * 100
            recall = true_positive / total_relevant * 100
            try:
                f1 = 2 * (precision * recall)/(precision + recall)
            except ZeroDivisionError:
                f1 = 0
            result = {
                'page': page,
                'total_relevant': total_relevant,
                'true_positive': true_positive,
                'false_positive': false_positive,
                'total_matched': true_positive + false_positive,
                'false_negative': total_relevant - true_positive,
                'precision': round(precision),
                'recall': round(recall),
                'f1': round(f1),
            }
            raw_results.append(result)
        return raw_results

if __name__ == '__main__':
    base = {'name': 'base', 'results': parse_csv('results/base.csv')}
    plural = {'name': 'plural', 'results': parse_csv('results/plural.csv')}
    synonym = {'name': 'synonym', 'results': parse_csv('results/synonym.csv')}
    all_matchers = {'name': 'all', 'results': parse_csv('results/all.csv')}

    top_matter = string.Template('\\begin{tabular}{${table_spec}}')
    line_format = '{page} & {total_relevant} & {true_positive} & {false_positive} & {total_matched} & {false_negative} & {precision} & {recall} & {f1} \\\\\\hline\n'
    heading_format = string.Template('\\multicolumn{${cols}}{|c|}{${name}}\\\\\\hline\n')
    table_spec = '|spec|'
    for i in base['results'][0].keys():
        table_spec = table_spec.replace('spec', ' c |spec')
    table_spec = table_spec.replace('|spec|', '|')
    result = top_matter.substitute(table_spec=table_spec) + '\\hline\n'

    for result_set in [base, plural, synonym, all_matchers]:
        result = result + heading_format.substitute(cols=len(result_set['results']) + 2,
                                                 name=result_set['name'].title())
        result = result + line_format.format(page='Page',
                                             total_relevant='Total Relevant',
                                             true_positive='True Positive',
                                             false_positive='False Positive',
                                             total_matched='Total Matched',
                                             false_negative='False Negative',
                                             precision='Precision \\%',
                                             recall='Recall \\%',
                                             f1='f\\textsubscript{1}\\%')
        for r in result_set['results']:
            result = result + line_format.format(**r)

    result = result + '\\end{tabular}'
    fp = open('build/resultstable.tex', 'w')
    fp.write(result)
    fp.close()


