\section{Introduction}
\label{sec:Introduction}
Textbooks are used to convey knowledge. With the progression of the digital age,
textbooks have frequently become digital. However, these digital textbooks often
take the form of a scanned or typed copy of the physical book and thus, do not
take advantage of new techniques that digital books can bring. These techniques
include text annotation, context-based navigation and automatic question and
answer generation~\cite{chaudhri2013}. The inclusion of new digital techniques in
digital textbooks has been shown to improve students' performance in homework
tasks by approximately 10\%~\cite{chaudhri2013}. ``Inquire Biology'' is the only
example of such a knowledge-driven digital textbook that currently exists.
However, it is proprietary and is very specific to a single edition of a single
textbook.

Thus, there is a lack of a generalised smart textbook platform that can
be used to interact with a wide variety of textbooks. A generalised platform
could help bring the academic improvements observed by Chaudhri et al. (2013)
using ``Inquire Biology'' to other disciplines and textbooks without
needing to re-engineer the combination of textbook and ontology for each
textbook. This software development project aims to develop such a generalised
smart textbook platform. Lodestar is a web-based platform for reading and
managing knowledge-driven textbooks.

The knowledge base used in Lodestar takes the form of an ontology, which is a
formal representation of concepts and their relationships~\cite{gruber1993}. In
the context of smart textbooks, the ontology is a graph with nodes representing
concepts and edges representing the relationships between concepts. A concept
can also have properties, which are attributes specific to that concept. For
example, a concept can have a definition property that contains the definition
of the concept in relation to the ontology domain.

Lodestar uses an ontology to provide text annotation of textbooks. A text
annotation is additional information about a word or phrase in the text that is
displayed on-screen to supplement the text itself. In the case of Lodestar,
these text annotations take the form of pop-up definitions of words and phrases
with a list of related concepts. It achieves a precision of 89\% and a recall of
70\% when matching terms in the Data Mining for the Masses
textbook~\cite{north2012} to items in the Data Mining Optimized (DMOP)
Ontology~\cite{keet2015}. This is an improvement on the current state of the
art. Recall is the ratio of correct positive predictions to total positive
elements, while precision is the ratio of correct positive predictions to total
predictions made by the system.

Lodestar performs text annotation by matching words and phrases in a textbook to
element names in the ontology. The matching process progresses through various
known generalized matching patterns. Initially single words are matched (with
the assumption that the element name is singular). Next phrases are matched.
If, after these two matchers, there are still no matches, words in the text are
converted to singular form and the single word matching occurs again against
these singular words. Finally, synonyms for the element name are generated using
WordNet~\cite{miller1995}, a lexical database for English, and these are matched
against the text using the single word matchers.

This paper is laid out as follows: Section~\ref{sec:Background} investigates the
existing literature on the subject of smart digital textbooks,
Section~\ref{sec:Design} describes the software design of Lodestar and the
development process that was followed, while Section~\ref{sec:Implementation}
describes how that design was accomplished in the final software project.
Section~\ref{sec:Evaluation} describes how Lodestar compares to other annotation
systems and Section~\ref{sec:Conclusions} pulls conclusions about Lodestar from
these results and contemplates additional work that could further improve Lodestar.
