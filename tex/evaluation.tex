\section{Evaluation}
\begin{table*}[h]
  \centering
  \input{build/resultstable}
  \caption{Full results for annotation using Lodestar's annotation system. Base
  refers to the base set of matchers, matching single words and phrases. Plural
  refers to the base set of matchers and the plural matcher. Synonym refers to
  the base set of matchers and the synonym matcher. Finally, All refers to all
  matchers: base, plural and synonym combined.}
  \label{tab:Results}
\end{table*}

\label{sec:Evaluation}
\begin{figure*}
  \centering
  \includegraphics{build/results}
  \caption{The average precision, recall and F\textsubscript{1} results over all
  7 pages for each of the four cases.}
  \label{fig:Results}
\end{figure*}

\subsection{Materials and Methods}
We evaluated Lodestar's annotation by comparing standard F\textsubscript{1},
precision and recall measures~\cite{cleverdon1966,vanrijsbergen1979} between
four variations of Lodestar's matching algorithm. The four variations were:
\begin{description}
  \item{Base case} — matching words and phrases,
  \item{Plurals} — the base case and plurals retrieved from WordNet,
  \item{Synonyms} — the base case and synonyms retrieved from WordNet, and
  \item{All} — all three matchers three combined.
\end{description}

Precision is a measure of how many annotated elements are relevant
\[p = \frac{|{\text{relevant elements}}\cap{\text{total
matched}}|}{|{\text{total annotated}}|}\]
Recall is a measure of how many relevant elements are annotated
\[r = \frac{|{\text{relevant elements}}\cap{\text{total
matched}}|}{|{\text{relevant elements}}|}\]
F\textsubscript{1} is the harmonic mean of the precision and recall, giving a
measure of the annotation's overall accuracy.
\[F_1 = \frac{2}{p^{-1} + r^{-1}}\]

Lodestar's annotation system was tested with 7 pages selected at random from the content
sections of the ``Data Mining for the Masses'' textbook~\cite{north2012}. The
content sections are the chapters in the textbook with actual content. This
excludes pages such as the title page and acknowledgements, ensuring that the
pages contain content relevant to the subject matter. This was done by noting
the first page of the first chapter and the last page of the final chapter and
using a random number generator to select 7 pages within these bounds. The
selected pages were 15, 65, 66, 81, 10, 227, 239. These page numbers are the
numbers listed on the pages in the physical copy, or the bottom of each page in
the PDF. The ontology used was the Data Mining Optimization (DMOP)
Ontology~\cite{keet2015}.

Each of the seven pages was converted into HTML using the Lodestar PDF parser. They
were then run through the four analyzers: base, plural, synonym and all. The
total relevant elements in the text were curated by the researchers. We manually
annotated the pages, selecting terms that are relevant to data mining, such as
data set and annotation. We further selected terms that existed in the ontology,
such as data. After Lodestar annotated the text, we compared the elements it
annotated with the elements we selected to remove any false negatives in our
manual annotation. This was done to produce the total relevant term counts
listed in Table~\ref{tab:Results}. Lodestar's annotation was then tallied to
produce the results in Table~\ref{tab:Results}.

\subsection{Results and Discussion}
\label{sec:Results}
Lodestar experimental results using precision, recall and F\textsubscript{1} are
given in Figure~\ref{fig:Results}. Table~\ref{tab:Results} shows the full
results.

From the results in Figure~\ref{fig:Results}, the base and plural matcher
performs better than purely the base alone, achieving a precision of 89\% and a
recall of 70\%. These results are encouraging, when considered in relation to
those achieved by other annotation systems such as Ontea. This is important
considering that Ontea's results were produced from an ontology generated from
the text it was annotating, while Lodestar used a separately created ontology
and text. Further study into the performance of Lodestar compared to other
annotation systems is needed to determine exactly how it compares. This is due
to the fact that the data set used by Laclavik, et al. (2009) is no longer
available and neither is the source code. Thus, making it impossible to annotate
the pages using Ontea.

The less effective recall performance in Lodestar could be an artefact of the
automatic generation of regular expressions for matching. The automatic nature
of the generation reduces Lodestar's ability to handle corner cases in the text
and ontology. An example is the class ``CorrelationSimilarity'' in the DMOP
ontology. This does not match ``correlation'' in the text as it does not know to
match only half the element name. It would match ``correlation similarity'',
however, as it is not capable of breaking the element name up and matching it
against a multi-word term in the text. It is likely that comparing only part of
the element name would result in a high number of false positives, reducing the
precision.

Some false positives include words like ``project'' and ``course'',
which the ontology uses as nouns, while in the text they are used as verbs
(project) or colloquially (course). Further, while still the same part of
speech, some matches are used in different ways in the text and ontology. For
example, the word ``series'' in the text: ``This is the participant's
performance on a series of responsiveness tests.'' (page 107 of Data Mining for
the Masses).  The DMOP ontology uses the word ``Series'' in the statistical
sense, to mean a list of numbers, while the textbook means a list of tests.

The synonym matcher reduces the accuracy of the base-case matches by 21\%. This
could be due to synonyms not taking into account what part of speech the term is
or synonyms being too generalised or common to be of use. This can be seen by
comparing the precision of the base-case and that of the synonym matcher. There
are numerous false positive matches found by using synonyms, such as ``perform''
and ``very''. This is further confirmed in the synonym matcher results in
Table~\ref{tab:Results}, where the high number of false positives on each page
are recorded. The increase in recall shows that there is a benefit to matching
synonyms if the false positives can be identified and removed, as it does match
more of the positive elements. This could be achieved by integrating a part of
speech tagger into the synonym matcher, but due to time limitations, this is yet
to be implemented in Lodestar.

There was also some synonyms that were missed, such as ``attribute'', used in
the textbook, as a synonym of ``feature'' in the ontology. This missed synonym
was due to WordNet not including ``attribute'' as a synonym for ``feature'',
which could be a limitation of WordNet's breadth around scientific
subjects~\cite{magnini2002}.

Matching plurals improves the annotation for both precision and recall. This is
expected, as plurals do not suffer the same generalisation issue that synonyms
do. Plurals continue to use the same base word as the singular version, thus
ensuring that common words are not also matched.
