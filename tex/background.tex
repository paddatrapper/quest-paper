\section{Background}
\label{sec:Background}
Textbooks have a long history of use by students and educators alike to cover
material and plan lessons~\cite{moulton1994,fuller1993}. They are considered an
essential learning tool, no matter the manner in which it is used within the
classroom~\cite{rockinson2013}. They have also been shown to contribute positively to
students' academic performance in developing countries. This is due to the fact
that a textbook can provide a more comprehensive coverage of the content than a
teacher may time to go through in the classroom~\cite{lockheed1986}.

In recent years, universities are increasingly using digital textbooks to offer
cost-effective, efficient and accessible resources to students~\cite{ross2017}.
Studies have found that South African students read digital textbooks at the
same speed as they read hard-copies, while maintaining the same comprehension
levels as when they were using hard-copies~\cite{sackstein2015}. However,
Sackstein et al. (2015), hypothesized that reading speed could be influenced by
students' prior exposure to the medium that they were reading
on~\cite{sackstein2015}. Thus, students' reading speed could solely be because
they are familiar with the platform, and the study results could be hiding
potential issues, such as students presenting lower comprehension levels and
slower reading speeds that students unfamiliar with digital textbooks could
encounter while using them.

It has been noted that digital textbooks are easier to update and distribute via
the internet~\cite{dlodlo2012, chowdhury2012}. Frequent updates are required for
subjects such as taxation tables and case-law where new standards are often
being set. This easy updating of digital textbooks also reduces the number of
different editions of textbooks students need to purchase, minimising the cost
incurred while studying~\cite{duplessis2014}. A web-based platform is able to
provide updates quickly to all its users as all textbooks and associated data
are located in one location controlled by the maintainer of the platform, thus
minimising the number of editions that students are required to use.

\subsection{Digital Smart Textbooks}
A digital smart textbook makes use of adaptive e-learning techniques to enhance
a textbook and to encourage students to read the textbook actively. Active
reading is the practice of summarizing, questioning, clarifying and predicting
when learning a concept. Palinscar and Brown (1984) found that active learning,
which active reading is a part, significantly improved students' comprehension
of the content being studied. Students were also able to generalize these gains
to class tests, thus improving their academic results~\cite{palinscar1984}.
Further, Chaudhri, et al. (2013) found that including adaptive e-learning into a
digital textbook, ``Inquire Biology'', improved students' performance in
homework tasks by approximately 10\%~\cite{chaudhri2013}. Adaptive e-learning
techniques include concept summaries, navigation history, asking and answering
students' questions and in-line definitions of concepts~\cite{chaudhri2013}.

\begin{figure}
  \input{images/concept_summary.pdf_tex}
  \caption{A graphical outline of what a differential concept summary between
  two concepts would look like.}
  \label{fig:ConceptSummary}
\end{figure}

Concept summaries are generated summaries of a single concept in the textbook.
It includes the definition of the concept, key facts, and relationships with
other concepts. Each linked concept then has its own concept summary. Concept
summaries are independent of chapters and pull knowledge from different chapters
into a single place, thus helping students to read actively. Active reading
occurs through enabling students to clarify and summarize the content around a
concept. These concept summaries are important as they clearly summarize the
content without requiring the student to wade through irrelevant information as
they would if they were searching through other resources such as
Wikipedia~\cite{chaudhri2013}. Figure~\ref{fig:ConceptSummary} shows a concept
summary displaying a concept with the differences with a linked concept
highlighted.

A potential limitation of a generated concept summary is that students lose any
advantages gained by writing the summary themselves, as summarizing a piece of
text is the first step of active reading~\cite{palinscar1984}. While a generated
concept summary does not prevent students from creating a summary of the content
themselves, it may make students less likely to do so.

In-line definitions of concepts reduce students' cognitive load when
working through the textbook by providing easy access to basic
terminology~\cite{chaudhri2013}. Students experience high cognitive load when
learning new concepts~\cite{sweller1988}. Reducing this load improves concept
acquisition. This is a form of active reading and thus has been shown to improve
academic results~\cite{palinscar1984}. ``Inquire Biology'' provides these
in-line definitions by underlining the term and providing a pop-up dialogue
containing the definition when the student mouses over the term. This pop-up is
useful as it allows students to skip over the definition if they already know
it, while giving them the opportunity to test their knowledge of the definitions
if they so choose~\cite{chaudhri2013}. Thus, further benefiting the learning
process.

\subsection{Text Annotation}
Ontology-based text annotation is the process of mapping strings in text to
ontology elements based on the provided subject domain ontology. This is done
through detecting metadata and structuring the data for improved processing
later~\cite{laclavik2009}. Annotation is important, as this is how the textbook
and ontology are combined to be presented to the user in a smart digital
textbook platform: the ontology provides a semantic interpretation of the
textbook~\cite{spasic2005}.

The main link between an ontology and its text is a terminology, which maps
concepts in the ontology to terms in the text. However, usually there is no
simple one-to-one mapping between terms and concepts. According to Spasic et
al. (2005), the probability of two experts to refer to the same concept with
the same term is less than 20\%. Another potential ambiguity is when the
same term refers to multiple concepts depending on the context in which it is
used~\cite{spasic2005}.

There are several text annotation tools available, including
Ontea~\cite{laclavik2009}, Textpresso~\cite{muller2018} and
GoPubMed~\cite{doms2005}. These all focus on using ontologies to enhance written
text and search results. While Ontea is generic to any piece of text and
ontology, Textpresso and GoPubMed both focus specifically on the biomedical
industry.

These tools are not suitable for use in Lodestar for several reasons. One reason
is that Ontea is domain specific in the standard regular expressions
provided~\cite{laclavik2009}. Another reason is that Textpresso and GoPubMed are
both highly specific to the biomedical field and would require an unjustifiable
amount of effort to generalize

Ontea uses regular expressions to match text to concepts in the linked
ontology~\cite{laclavik2009}. Laclavik, et al. (2009) achieved an average recall
of 79\% and an average precision of 53\% using Ontea, but required the user to
manually create the regular expressions to be used. Lodestar performed better,
with a recall of 70\% and a precision of 89\%.

Ontea's performance was measured on text using an ontology specifically tailored
to it. This would be expected to have a better precision and recall that using a
separately developed ontology with the same text and annotation tool as the
terms in the text and ontology are the same. Thus, improving the chance of
matching terms correctly.
