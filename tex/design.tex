\section{Design}
\label{sec:Design}
We aim to provide a digital smart textbook platform that enables educators and
students to interact with textbooks in a context-aware, smart way. Lodestar
allows administrators to upload textbooks and ontologies so that users can
access the smart textbooks once they are processed. These smart textbooks
include in-line definitions of concepts and related concept listing based on the
ontology provided.

After meeting with the University of Cape Town's (UCT) Centre for Innovation in
Learning and Teaching (CILT), we developed the following list of core requirements:

\begin{itemize}
  \item Lodestar should annotate uploaded source files with a precision and
    recall which is, at minimum, equivalent to existing annotation systems such as
    Ontea~\cite{laclavik2009},
  \item Administrators should be able to upload textbooks and ontologies,
  \item Users should be able to view textbooks after they are uploaded,
  \item Administrators should be able to added and remove users.
\end{itemize}

A list of desired additional features was also discussed, while these were not
implemented, they form a reference for what is still needed before Lodestar can
be used properly in a teaching environment. The list included:

\begin{itemize}
  \item Integration with existing Learning Management Systems,
  \item User friendly interface following standard human-computer interaction
    principles,
  \item Track student usage to see which sections are used more frequently and
    students' progress through the textbook,
  \item Allow the user to highlight text and make comments in the margin
\end{itemize}

\subsection{System Architecture}
We selected a web-based system, as this enables administrators to update and
maintain the textbooks provided easily. It further allows users to access the
textbooks from various locations on a variety of devices without needing to
develop specific applications for every anticipated platform. This reduces
complexity and aids future maintainability. It also enables easy, cost-effective
updating of textbooks by the administrators.

The platform is broken up into 4 parts: source parser, textbook annotation,
textbook rendering and administration. This pipeline design ensures modularity
and extendibility. Modularity allows the software to be altered with ease, as
there are well-defined boundaries between parts and internal changes to one will
not affect another. With this modularity, extending functionality is made
possible by adding additional parts to perform the required duties. This
extensibility helps iterative development and clean testing of changes.
Figure~\ref{fig:Pipeline} shows this pipeline process graphically.

\begin{figure*}
  \centering
  \def\svgwidth{\textwidth}
  \input{images/pipeline.pdf_tex}
  \caption{Lodestar textbook pipeline}
  \label{fig:Pipeline}
\end{figure*}

The source parser module handles the upload of source textbooks and ontologies
by the administrators. This is where supported formats are defined and metadata
about the files are gathered. As administrators will be uploading content which
is stored and processed on the server, there are security implications that need
to be considered. Some of these are the responsibility of the server
administrator, however some are considered within the software architecture chosen
for Lodestar. Data security and server maintenance is the responsibility of the
system administrator, while Lodestar must ensure that uploaded data do not
present additional security risks to the system.

Uploaded textbooks are expected to be uploaded in one of the supported formats.
If the file is not one of these, the upload fails. This approach reduces
attack surface that uploading files provides. In order to reduce it further,
files are converted to plain Hypertext Markup Language (HTML) files prior to
further processing. This ensures that there is no malicious code that can be
executed on the server. Using HTML as the intermediary file format ensures that
the file is suitable for rendering on-screen even if no annotations are found.

Textbook annotation is the second step in the pipeline. This is where text
annotations are added, which is done to reduce the processing power required to
handle each client request that is sent to the renderer. This method increases
the size of each textbook on disk, but the trade-off is that the time required
to process requests is much smaller. The space-time trade-off is justified by
the ratio of expected client requests to source file uploads. Each user will
generate a request for every textbook viewed. Thus, requests are handled
frequently, while the textbook should only be uploaded irregularly. Thus,
processing the textbook each time a user wishes to view it would increase request
handling time, making the site slow and unresponsive. In Lodestar, the textbook
does not change on each request, rather on each upload of a new source file.
This makes processing and storing the changes more efficient.

Once the textbook is processed, it can be displayed to the users in the
rendering module. This module is responsible for the look and feel of the
website as experienced by end users. The standard user view is a list of
textbooks that they can access. Administrators have an additional link on the
home screen that allows them to access the administration module.

The administration module is responsible for allowing administrations to
maintain and update textbook and ontology details. This is also the entry point
for adding new source textbooks and ontologies. Furthermore, the administration
module allows administrators to manage users and user permissions on the
website.

In every module, there is a clear distinction between the model, view and
controller logic. In the Model/View/Controller (MVC) design pattern, the view
displays information to the user, while the model is the internal representation
of the data. The controller processes user input and interaction between the
view and the model~\cite{leff2001}. This separation makes maintaining multiple
interfaces or changing the interface easy, without needing to change the
internal representation of data. In iterative development this property is very
useful, as it decouples the model, view and controller. This allows iterations
to modify any one of the three easily without needing to change the others
significantly.

\subsection{Software Development}
We chose to use an iterative development process. This process initially
implements a skeletal sub-project. Additional features and enhancements are
then added in future iterations~\cite{basil1975}. Iterative development was
chosen as it allows for flexibility in the implementation of features and does
not need the team which is required in agile-based software development
processes. The flexibility in implementation stems from the iterative nature of
the process.  After each implementation phase, the current implementation is
analysed and this dictates on what the next implementation phase will
focus~\cite{basil1975}.

The development cycle also followed Test Driven Development (TDD). TDD
requires writing automated tests before developing the features that they test.
Thus, initially, the tests are written and fail. The feature is then implemented
and the tests run again. If the feature is implemented correctly, the tests
should then pass~\cite{janzen2005}. TDD provides rapid, reproducible feedback on
the state of the project. It also ensures that the test suite covers the entire
project at a low level, thus ensuring external consistency when changes are
made. However, as it tests features at a unit (method or class) level, it does
not test for integration issues when multiple units interact in any complex way.
These situations also need to be tested in an automated way to reduce the chance
of regressions appearing.

Lodestar is licensed under the GNU General Public Licence (GPL) version 3 with
the clause allowing redistribution under a later version of the licence at the
distributor's discretion. This is an open source licence that allows users to
modify and redistribute the modifications, provided that they release their
modifications under compatible licensing terms~\cite{pearson2000}. Open source
licensing such as the GPL ensures that, should the original authors stop
developing the platform, interested stakeholders can continue on without needing
to start from scratch. Users who require specific features for their environment
can also freely make modifications. The source code is available at
\url{https://gitlab.com/paddatrapper/lodestar}.

As the software needs to store user data, it must comply with the South African
Protection of Personal Information (POPI) Act. This outlines that only data that
are relevant be collected and that the user must consent to this data being
collected before it can be done. Furthermore, data must only be used for the
purposes that it was originally intended for and there must be reasonable
safeguards to ensure the data's safety~\cite{debruyn2014}. In Lodestar, this is
done by never storing sensitive information, such as passwords, in plain-text.
Instead, they are cryptographically encrypted before storing. The permissions
structure also ensures that only registered and authorized users can access
personal data. The system administrator can also ensure compliance by
applying encryption between users and the server running Lodestar, and
implementing security measures on the server to prevent unauthorized access.
