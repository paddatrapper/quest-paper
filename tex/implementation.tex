\section{Implementation}
\label{sec:Implementation}
We chose to implement Lodestar in Python using the Django web
framework\footnote{\url{https://www.djangoproject.com/}}. This decision was made
as Python is cross-platform and can be run on Windows, Mac and
Linux~\cite{sanner1999}. This allows Lodestar to be developed and deployed on any
platform, which ensures flexibility. Django also provides all the logic required
to translate between web requests, responses and Python without us needing to
re-implement it. TDD is supported through a structured testing framework
implemented inside Django that allows tests to execute functions and construct
classes in the same way as the Django engine does when serving web requests. It
further allows testing of requests and responses at the web level. Thus, it
makes performing integration testing of multiple components simpler. This
improves efficiency and makes writing tests less of a burden.

Lodestar consists of four modules: the source parser, textbook annotation,
rendering and administration. The source parser, textbook annotation and
rendering module form a pipeline, with the administration module providing user
management, authentication and a permissions structure.

Progression through the processing pipeline is done using signals. When a source
file is uploaded by an administrator and accepted by the server, it is saved
into the database. This emits a signal to the source upload stage of the
pipeline. Once this is complete, it emits a signal to schedule the textbook
processing to occur in the background. The textbook processing takes place in the
background so that the user does not have to wait for what can be a lengthy
process, depending on the size of the ontology and length of the textbook. Their
upload is accepted immediately, and scheduled for processing. The user can then
continue using Lodestar or close their browser and the processing will still
occur. The textbook will only be available once the processing is complete.

\subsection{Source Upload}
The source upload process needs to be tailored specifically for each supported
file format. This is due to the fact that the different formats need to be
processed into a common format that can then be annotated and rendered to users.
Efficient processing of uploads was achieved through a hash table mapping file
type to transformation function. A hash table provides easy extensibility by
adding additional entries for new file formats as they are implemented. In order
to improve maintainability, all source format parsers inherit from a single base
class that is responsible for registering file types in the hash table and
provides common code to reduce duplication.

Lodestar currently only supports two file types: Portable Document Format (PDF)
and HTML. This limitation is due to a lack of feasibility with the time
available for developing the initial version, and more formats can be added in
the future. The PDF parsing function converts the source file into an HTML file
and performs a variety of regular expression string replacements on it to remove
illegal characters and ensure that it is formatted correctly for rendering.
Conversion from PDF to HTML is performed by the pdftotext application from the
Poppler\footnote{\url{https://poppler.freedesktop.org/}} project. The HTML
parsing function assumes that the file is valid HTML and ensures that it is
copied into the correct directory with the correct name.

Once the source is uploaded and converted into a format that the processing
pipeline can manipulate, it schedules the textbook for processing in the
background.

Ontologies are uploaded as OWL/XML or RDF/XML files. When uploading, the
administrator can add details about the ontology, including a list of the
properties to use as definitions. A concept has a definition that can be added
to an annotation if the concept has a value for at least one of these
properties. The list of definition properties is a comma separated list. The
last property in the list with a value found for each object in the ontology is
used as the concept's definition. Once an ontology is uploaded, textbooks can be
uploaded linking to it. This achieves our requirement of allowing administrators
to upload textbooks and ontologies.

\subsection{Textbook Annotation}
Once the textbook annotation stage is started, it is responsible for annotating
the HTML file so that the renderer can display in-line definitions and term
relations. It processes the HTML line-by-line, finding words or phrases in the
text that matches element names from the ontology and adds the required details as
an HTML element to the text. The annotated line is then written to an HTML file
that is later displayed to the user. This is explained in
Algorithm~\ref{alg:Annotation}. The algorithm uses owlready~\cite{lamy2017} to
interact with the ontology.

\begin{algorithm}
  \caption{Annotation of a textbook}
  \label{alg:Annotation}
  \begin{algorithmic}[1]
    \REQUIRE $ontology, sourceFile, htmlFile$
    \STATE $g \aeq \text{loadOntology}()$
    \STATE $predicates \aeq ontology.predicates$
    \STATE $definitionMap \aeq \text{getDefinitions}(g, predicates)$
    \label{alg:Annotation:getDefinitions}
    \WHILE{$sourceFile$ has next line}
    \STATE $line \aeq sourceFile.\text{readLine}()$
    \FORALL{$subject, definition$ in $definitionMap$}
    \STATE $match, matchTerm \aeq \text{getMatch}(subject, line)$
    \label{alg:Annotation:getMatch}
    \IF{$match$}
    \STATE $line \aeq line.\text{replace}(matchTerm, match)$
    \label{alg:Annotation:replace}
    \ENDIF
    \ENDFOR
    \STATE $htmlFile.\text{write}(line)$
    \ENDWHILE
  \end{algorithmic}
\end{algorithm}

Algorithm~\ref{alg:Annotation} requires the file path to the source HTML file
($sourceFile$). This is the HTML file generated by the source upload module from
the PDF or HTML file that the administrator uploaded. $htmlFile$ is the file
path that the annotated text will be written out to. \mbox{getDefintions} on
line~\ref{alg:Annotation:getDefinitions} creates a map of elements in the
ontology to definitions. This is a hash map of element names to definitions,
stored in $definitionMap$. If the concept does not have a definition, it is
added, but the definition is blank. A concept has a definition if it has a
definition predicate or property. These are the list of properties given by the
administrator when they uploaded the ontology. The line test function then uses
this filtered map of classes to definitions to detect terms.

\mbox{getMatch} on line~\ref{alg:Annotation:getMatch}
(Algorithm~\ref{alg:Annotation}) calls the function described in
Algorithm~\ref{alg:Matching}, which matches terms in the ontology to words in a
line using a series of matching functions. These match terms, then single words
and finally try variations on words such as plurals and synonyms using
WordNet~\cite{miller1995}, an online lexical database. The plural and synonym
matchers are described in Algorithms~\ref{alg:Plural} and~\ref{alg:Synonym}
respectively. This design allows the easy addition of new matchers by adding
them to the list of matchers. \mbox{getMatch} (Algorithm~\ref{alg:Matching})
returns two data: the regular expression generated to match the term in the line
and the HTML element to replace it with. This HTML element includes sections of
the definition and related concepts so that they can be displayed when the
textbook is rendered and is generated by \mbox{getItem} on
line~\ref{alg:Matching:formatItem} (Algorithm~\ref{alg:Matching}). The regular
expression is used on line~\ref{alg:Annotation:replace}
(Algorithm~\ref{alg:Annotation}) to replace the term with the new HTML element.
This regular expression is generated to ensure that the term is matched in the
correct location in the line. For example if a previous term contains the
current term in the definition, the regular expression must not match the term
in the definition, rather the one in the original text.

\begin{algorithm}
  \caption{Matching a term in a line of text}
  \label{alg:Matching}
  \begin{algorithmic}[1]
    \REQUIRE $matchers, term, line, predicates$
    \FORALL{$matcher$ in $matchers$}
    \STATE $stop, regex, match \aeq matcher(term, line)$
    \IF{``stop'' in $result$}
    \STATE $item \aeq \text{formatItem}(match, predicates)$
    \label{alg:Matching:formatItem}
    \RETURN $regex, item$
    \ENDIF
    \ENDFOR
    \RETURN None
  \end{algorithmic}
\end{algorithm}

In Algorithm~\ref{alg:Plural}, WordNet is used to split the line up into words
(line~\ref{alg:Plural:tokenize}) and then to generate the singular versions of
each word (line~\ref{alg:Plural:getSingleEach}). These singular versions are
then matched against the term, ignoring letter case.

\begin{algorithm}
  \caption{Matching a plural of a term}
  \label{alg:Plural}
  \begin{algorithmic}[1]
    \REQUIRE $term, line$
    \STATE $lineList \aeq wordnet.\text{tokenize}(line)$
    \label{alg:Plural:tokenize}
    \STATE $lineListSingle \aeq wordnet.\text{getSingleEach}(lineList)$
    \label{alg:Plural:getSingleEach}
    \FOR{$i = 0$ \TO length$(lineListSingle)$}
    \IF[Case insensitive match]{$lineListSingle[i] = term$}
    \RETURN ``stop'',\\``\textbackslash b'' + $lineListSingle$ +
    ``\textbackslash b(?![\textbackslash s\textbackslash w]*</span>)'',\\
    $lineList[i]$
    \ENDIF
    \ENDFOR
    \RETURN None
  \end{algorithmic}
\end{algorithm}

WordNet is used again in Algorithm~\ref{alg:Synonym} to retrieve a list of
synonyms for the term. A limitation of this approach is that WordNet is unable
to determine which part of speech the term is, often resulting in many synonyms
with the incorrect part of speech being returned and subsequently matched. WordNet
is also limited in its breadth around scientific subject, thus often resulting
in generic or incorrect (in context) synonyms being returned~\cite{magnini2002}.

\begin{algorithm}
  \caption{Matching a synonym of a term}
  \label{alg:Synonym}
  \begin{algorithmic}[1]
    \REQUIRE $term, line$
    \STATE $synonyms \aeq wordnet.\text{synonyms}(term)$
    \label{alg:Synonym:synonyms}
    \FORALL{$synonym$ in $synonyms$}
    \IF[Case insensitive match]{$synonym = term$}
    \RETURN ``stop'',\\``\textbackslash b'' + $term$ +
    ``\textbackslash b(?![\textbackslash s\textbackslash w]*</span>)'', $synonym$
    \ENDIF
    \ENDFOR
    \RETURN None
  \end{algorithmic}
\end{algorithm}

As an example of the matching process, the following HTML paragraph and an
ontology with the elements given in Table~\ref{tab:ExampleOntology} can be
combined by running Algorithm~\ref{alg:Annotation}. The concepts in
Table~\ref{tab:ExampleOntology} come from the Data Mining Optimization (DMOP)
ontology~\cite{keet2015}.
\begin{verbatim}
<p>
  A data set is required to perform analysis of data in order
  to make predictions
</p>
\end{verbatim}
This produces the following output (additional formatting added for
readability):
\begin{verbatim}
<p>
  A <span class="popup" onclick="showPopUp('definition-0')">
    <span id="class-0">data set</span>
    <span class="popup-text" id="definition-0">
      DataSet: in data mining, the term data set is
      defined as a set of examples or instances
      represented according to a common schema.
    </span>
    <span class="popup-relations" id="relations-0">
      <span class="popup-relation" id="relation-0">
        <span class="relation-name">type</span>
        <span class="relation-obj">Data</span>
      </span>
    </span>
  </span> is required to perform analysis of
  <span class="popup" onclick="showPopUp('definition-1')">
    <span id="class-1">data</span>
    <span class="popup-text" id="definition-1">
      Data is the generic term that englobes
      different levels of granularity: data can be a
      whole dataset (one main table and possibly
      other tables), or only a table, or only a
      feature (column of a table), or only an
      instance (row of a table), or even a single
      feature-value pair.
    </span>
    <span class="popup-relations" id="relations-1">
      <span class="popup-relation" id="relation-0">
        <span class="relation-name">type</span>
        <span class="relation-obj">
          non-physical-endurant
        </span>
      </span>
    </span>
  </span> in order to make predictions
</p>
\end{verbatim}

\begin{table}
  \centering
  \begin{tabular}{| c | p{11em} | p{9em} |}\hline
    Concept & Definition & Related Elements \\\hline
    DataSet & DataSet: in data mining, the term data set is defined as a set of
    examples or instances represented according to a common schema. & type: Data
    \\\hline
    Data & Data is the generic term that englobes different levels of
    granularity: data can be a whole dataset (one main table and possibly other
    tables), or only a table, or only a feature (column of a table), or only an
    instance (row of a table), or even a single feature-value pair. & type:
    non-physical-endurant \\\hline
  \end{tabular}
  \caption{A list of example concepts in an ontology. The related elements are
  elements in the ontology, and are not necessarily listed here.}
  \label{tab:ExampleOntology}
\end{table}

\subsection{Textbook Rendering}
\begin{figure*}
  \centering
  \includegraphics[width=\textwidth]{images/screenshot.png}
  \caption{The user interface of Lodestar showing the underlined terms and the
  information pop-up for techniques, which is shown when the term is clicked
  on.}
  \label{fig:Screenshot}
\end{figure*}
The processing pipeline produces an annotated HTML file for each textbook
uploaded. The rendering module embeds this into the website to ensure a
consistent look and feel. Consistency is used to ensure that the user feels
comfortable while using the website. The user is presented with a list of textbooks
and when one is selected, the renderer initially displays just the textbook's
content on the screen. Terms are underlined and the user can click on them to
access the definitions and related concepts. This information is presented in a
side panel that pops up next to the text on the same page, which is consistent
with other websites, such as GitLab and Facebook, where extra information is
presented to the side of the main content. It also ensures that users do not
have to switch between screens to view definitions and the textbook, making the
website more user-friendly. By displaying textbooks to the user, we ensure that
our requirement for users being able to view textbooks is met.
Figure~\ref{fig:Screenshot} is what the user sees when viewing a textbook on
Lodestar.

All styling and manipulation of the web-page is done using standard web
technologies, namely CSS, JavaScript, jQuery\footnote{\url{https://jquery.com/}}
and HTML~\cite{lauinger2018}. This ensures that the renderer can be maintained
easily in the future as these technologies already drive the majority of
websites~\cite{lauinger2018}.

The administration section of the rendering module allows administrators to add,
delete and modify textbooks and ontologies. Unnecessary re-processing of
textbooks is prevented by only executing the processing pipeline when the
textbook source file changes. This is done by storing the MD5 message digest
hash and comparing this to the MD5 hash of new file uploads. The MD5 hash is a
128 bit hash of a file that is typically unique~\cite{rivest1992}. While it is
not cryptographically secure~\cite{wang2005}, it is sufficient to minimise
unnecessary re-processing of textbook source files. MD5 is not cryptographically
secure because two files can occasionally produce the same MD5 hash even if their contents
are different. In Lodestar, this will only cause the updated textbook not to
process the new source, but this can be started manually by an administrator
from the detail view of a textbook. The advantage of MD5 is that it is fast and
not computationally taxing to compute~\cite{rivest1992}. Reducing the number of
times a textbook is processed reduces the load on the server running Lodestar and
thus, improves the user experience.

The administration section further enables the administrator to add and remove
users and manage their respective permissions. This ensures that our user
administration requirement is met.
