COMPILER = pdflatex
BIBC = bibtex
EPSC = epspdf
BUILD_DIR = ./build
SEC_DIR = ./tex
GRAPHICS_DIR = ./images
SRC = $(wildcard *.tex)
DEP = $(wildcard $(SEC_DIR)/*.tex)
EPS = $(wildcard $(GRAPHICS_DIR)/*.eps)
PDF = $(patsubst $(GRAPHICS_DIR)/%.eps, $(BUILD_DIR)/%.pdf, $(EPS))
OUTPUT = $(SRC:tex=pdf)
TEX_OPT = -halt-on-error -output-dir=$(BUILD_DIR)

.PHONY: compile clean

compile: $(OUTPUT)

clean:
	rm -rf $(OUTPUT) $(BUILD_DIR)

$(OUTPUT): $(SRC) $(DEP) $(PDF) | $(BUILD_DIR)
	python3 generate_tables.py
	$(COMPILER) $(TEX_OPT) $(SRC)
	$(BIBC) $(BUILD_DIR)/$(SRC:tex=aux)
	$(COMPILER) $(TEX_OPT) $(SRC)
	$(COMPILER) $(TEX_OPT) $(SRC)
	mv $(BUILD_DIR)/$(OUTPUT) $(OUTPUT)

$(BUILD_DIR)/%.pdf: $(GRAPHICS_DIR)/%.eps | $(BUILD_DIR)
	$(EPSC) $< $@

$(BUILD_DIR):
	mkdir $(BUILD_DIR)
