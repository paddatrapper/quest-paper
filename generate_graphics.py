###########################################################################
# GenerateGraphics is Copyright (C) 2019 Kyle Robbertze <RBBKYL001@myuct.ac.za>
#
# GenerateGraphics is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# GenerateGraphics is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GenerateGraphics. If not, see <http://www.gnu.org/licenses/>.
###########################################################################
import matplotlib.pyplot as plt
import numpy as np
import math

def parse_csv(filename):
    raw_results = []
    with open(filename) as f:
        for line in f:
            if line.startswith('#'):
                continue
            values = line.split(',')
            page = int(values[0])
            total_relevant = int(values[1])
            true_positive = int(values[2])
            false_positive = int(values[3])
            precision = true_positive / (true_positive + false_positive) * 100
            recall = true_positive / total_relevant * 100
            try:
                f1 = 2 * (precision * recall)/(precision + recall)
            except ZeroDivisionError:
                f1 = 0
            result = {
                'page': page,
                'total_relevant': total_relevant,
                'true_positive': true_positive,
                'false_positive': false_positive,
                'precision': round(precision),
                'recall': round(recall),
                'f1': round(f1),
            }
            raw_results.append(result)
        return raw_results

def get_average_results(filename):
    raw_results = parse_csv(filename)
    total_relevant = 0
    true_positive = 0
    false_positive = 0
    for r in raw_results:
        total_relevant += r['total_relevant']
        true_positive += r['true_positive']
        false_positive += r['false_positive']
    precision = true_positive / (true_positive + false_positive) * 100
    recall = true_positive / total_relevant * 100
    f1 = 2 * (precision * recall)/(precision + recall)

    return {'precision': round(precision),
            'recall': round(recall),
            'f1': round(f1)}

def autolabel(ax, rects, font_size):
    for rect in rects:
        height = rect.get_height()
        ax.annotate('{}'.format(height),
                    xy=(rect.get_x() + rect.get_width() / 2, height),
                    xytext=(0,2),
                    textcoords='offset points',
                    ha='center', va='bottom', fontsize=font_size)

def set_params(font_size, fig_width_pt):
    inches_per_pt = 1.0 / 72.27
    golden_mean = (math.sqrt(5) - 1.0) / 2.0
    fig_width = fig_width_pt * inches_per_pt
    fig_height = fig_width * golden_mean / 2
    fig_size = [fig_width, fig_height]
    params = {
        #'backend': 'ps',
        'axes.labelsize': font_size,
        'font.size': font_size,
        'figure.titlesize': font_size,
        'axes.titlesize': font_size,
        'legend.fontsize': font_size,
        'xtick.labelsize': font_size,
        'ytick.labelsize': font_size,
        'text.usetex': True,
        'figure.figsize': fig_size,
    }
    plt.rcParams.update(params)

def add_rects(ax, x, data, label, width, font_size, style=None):
    rects = ax.bar(x, data, width, label=label)
    if style:
        for p in rects.patches:
            p.set_hatch(style)
    autolabel(ax, rects, font_size)

def generate_plot(results, dest):
    labels = ['Precision', 'Recall', 'F1']
    styles = [None, '//', '...', 'xx']
    font_size = 9
    fig_width_pt = 240.0 * 2
    set_params(font_size, fig_width_pt)

    fig, ax = plt.subplots()
    x = np.arange(len(labels))
    y = np.arange(0, 101, 10)
    width = 0.2

    i = 0
    pos_step = 1
    pos_base = -3/2 + 2 - 1/2 * len(results)
    for name, result in results.items():
        data = [result['precision'], result['recall'], result['f1']]
        add_rects(ax, x + (pos_base + pos_step * i) * width, data, name, width,
                  font_size - 2, styles[i % len(styles)])
        i += 1

    ax.set_ylabel('Percentage (\\%)')
    ax.set_xticks(x)
    ax.set_xticklabels(labels)
    ax.set_yticks(y)
    ax.legend(loc='upper left', bbox_to_anchor=(1.01, 1.0),
              borderaxespad=0, frameon=False)
    fig.tight_layout()
    fig.savefig(dest)

if __name__ == '__main__':
    avg_results = {
        'Base': get_average_results('results/base.csv'),
        'Plural': get_average_results('results/plural.csv'),
        'Synonym': get_average_results('results/synonym.csv'),
        'All': get_average_results('results/all.csv'),
    }

    generate_plot(avg_results, 'images/results.eps')
